## building instructions:

- add `/src/CrossRoads/Libraries/derby.jar` and `/src/CrossRoads/Libraries/derbytools.jar` to Java build path;
- download JavaFX SDK (https://openjfx.io/) and add 
`javafx-sdk-<VERSION>/lib/javafx.base.jar` and `javafx-sdk-<VERSION>/lib/javafx.graphics.jar` to Java build path.